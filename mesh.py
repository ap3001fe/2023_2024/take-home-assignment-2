from skfem import *
from skfem.visuals.matplotlib import plot as skplot
from skfem.visuals.matplotlib import plot3 as skplot3, show
from scipy.sparse import lil_matrix as sparse_matrix
from scipy.sparse.linalg import spsolve as solve
import numpy as np

class Mesh:
    
    def __init__(self,type_of_mesh: str,mesh=None,**kwargs: dict):
        
        if 'refinement' in [*kwargs]:
            self.refinement = int(kwargs['refinement'])
        else:
            self.refinement = 0
        
        
        if mesh != None:
            self.mesh = mesh
        else:
        
            if type_of_mesh == "circle":
                self.mesh = MeshTri1().init_circle().refined(self.refinement)

            elif type_of_mesh == "square":
                self.mesh = MeshTri1().refined(self.refinement)


            else:
                print("Error: domain type not recognized")
            
        self.points = self.mesh.p
        self.topology_edges = self.mesh.facets.T
        self.boundary_edges = self.mesh.boundary_facets()
        self.type_of_mesh = type_of_mesh
        self.topology_elements = self.mesh.t.T
    
    def refine(self,value: int):
        
        return Mesh(self.type_of_mesh, self.mesh.refined(value))
        
    def draw(self):
        ax = self.mesh.draw()
        ax.set_axis_on()
        
        return ax

def plot(mesh,u, shading='gouraud', colorbar=True):

    return skplot(mesh.mesh, u, shading='gouraud', colorbar=True)

def plot3d(mesh,u):
    
    return skplot3(mesh.mesh, u)


